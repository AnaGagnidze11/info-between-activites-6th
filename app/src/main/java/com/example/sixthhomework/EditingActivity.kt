package com.example.sixthhomework

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.RadioButton
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_editing.*
import kotlinx.android.synthetic.main.activity_main.*

class EditingActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_editing)
        init()
    }

    private fun init(){
        saveChangesButton.setOnClickListener {
            saveNewInfo()
        }
    }

    private fun saveNewInfo(){
        val name = firstNameEdtT.text.toString()
        val lastName = lastNameEdtT.text.toString()
        val email = emailEdT.text.toString()
        val birthYear = birthYearEdtT.text.toString()


        if (name.isNotEmpty() && lastName.isNotEmpty() && email.isNotEmpty() && birthYear.isNotEmpty() &&
                (Female.isChecked or Male.isChecked or Other.isChecked)){
                if (android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    val intent = intent
                    intent.putExtra("name", name)
                    intent.putExtra("lastName", lastName)
                    intent.putExtra("email", email)
                    intent.putExtra("birthYear", birthYear)

                    setResult(Activity.RESULT_OK, intent)
                    finish()

                }else{
                    emailEdT.error = "Email format is not correct"
                }
        }else{
            Toast.makeText(this, "Please fill all fields", Toast.LENGTH_SHORT).show()
        }
    }


    fun onRadioButtonClick(view: View){
        val sex:String
        if(view is RadioButton){
            val checked = view.isChecked

            when(view.getId()){
                R.id.Female -> if(checked){
                    sex = view.text.toString()
                    intent.putExtra("sex", sex)
                }

                R.id.Male -> if (checked){
                    sex = view.text.toString()
                    intent.putExtra("sex", sex)
                }

                R.id.Other -> if (checked){
                    sex = view.text.toString()
                    intent.putExtra("sex", sex)
                }
            }
        }
    }
}