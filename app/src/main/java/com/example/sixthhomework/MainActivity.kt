package com.example.sixthhomework

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    companion object{
        private const val REQUEST_CODE = 1
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.Theme_SixthHomework)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init(){
        editProfileButton.setOnClickListener {
            openEditor()
        }
    }

    private fun openEditor(){
        val intent = Intent(this, EditingActivity::class.java)
        startActivityForResult(intent, REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK){
            val name = data?.extras?.getString("name", "")
            val lastName = data?.extras?.getString("lastName", "")
            val email = data?.extras?.getString("email", "")
            val birthYear = data?.extras?.getString("birthYear", "")
            val sex = data?.extras?.getString("sex", "")

            nameTxtV.text = name
            lastNameTxtV.text = lastName
            emailTxtV.text = email
            yearTxtV.text = birthYear
            sexTxtV.text = sex
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}